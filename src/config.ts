// We use requirejs.config instead of require.config because adding the import hides the correct require implementation
requirejs.config({
    waitSeconds: 100,
    paths: {
        'index': 'deps/index'
    },
    shim: {}
});

// All external libraries to load with require.js
const EXTERNAL_LIBS: string[] = [
    'index'
];

const requireReadyFn: Function = () => {
    console.log('requireReadyFn');
};

require([...EXTERNAL_LIBS], requireReadyFn);