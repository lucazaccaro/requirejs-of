module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        ts: {
            default: {
                options: {
                    // Disable the grunt-ts fast feature 
                    fast: 'never'
                },
                // Specifying tsconfig as a boolean will use the 'tsconfig.json' in same folder as Gruntfile.js 
                tsconfig: true
            }
        }
    });

    grunt.loadNpmTasks('grunt-ts');
    
    // Default task(s)
    grunt.registerTask('default', 'Production Flow', ['ts']);
};
